/* eslint-disable no-unused-vars */
import { styled, Box } from '@mui/material'


// main container of background carousel 
export const Container = styled(Box)`
height:100vh;
width:100vw;
position:absolute;
top:0;
left:0;
z-index:-10;
filter:brightness(35%);
& > div{
    & img{
        height:100vh;
        width:100vw;
        object-fit:cover;
    }
}
`


