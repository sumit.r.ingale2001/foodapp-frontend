/* eslint-disable no-unused-vars */
import { styled, Box, Paper } from '@mui/material'

// login container 
export const Container = styled(Box)(({ theme }) => ({
    minHeight: "100vh",
    width: "100vw",
    display: "flex",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    padding:"5rem 1rem",
}))

export const StyledPaper = styled(Paper)(({ theme }) => ({
    padding: "1.5rem 2rem",
    maxWidth: "500px",
    background: "rgba(225,225,225,0.8)",
    backdropFilter: "blur(5px)",
    width: "100%",
    "& > h4": {
        fontWeight: "bold",
        marginBottom: "1.5rem",
        textAlign: "center"
    },
    "& > form": {
        display: "flex",
        flexDirection: "column",
        gap: "1rem",
        margin:"0.5rem 0"
    },
    "& > p":{
        textAlign:"center",
    }
}))

export const Bg = styled(Box)(({ theme }) => ({
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    zIndex: "-1",
    "& > img": {
        width: "100%",
        height: "100%",
        objectFit: "cover"
    },
    filter: "brightness(30%)",
}))
