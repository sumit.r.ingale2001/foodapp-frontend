import { useDispatch, useSelector } from "react-redux"
import { Container } from "./styles"
import { useEffect } from "react";
import { getAllProducts } from "../../store/builderFunctions";
import { Card, Footer } from "../../components";

const Menu = () => {

    const dispatch = useDispatch();
    const data = useSelector((state) => state.restaurants.allRestaurantProduct)


    useEffect(() => {
        if (data?.length === 0) {
            dispatch(getAllProducts())
        }
    }, [])

    return (
        <>

            <Container>
                {
                    data?.map((item) => (
                        item.products.map((itm) => (
                            <Card key={itm._id} data={itm} restaurantName={item.restaurantName} restaurantID={item.id} />
                        ))
                    ))
                }
            </Container>
            <Footer />
        </>
    )
}

export default Menu
