/* eslint-disable no-unused-vars */
import { useDispatch, useSelector } from "react-redux";
import { useGetRestaurantID } from "../../hooks/useGetUserID";
import { useEffect, useState } from "react";
import {
  deleteProduct,
  getSingleRestaurantProducts,
} from "../../store/builderFunctions";
import { Card, Footer } from "../../components";
import { Container } from "./styles";

const AllProducts = () => {
  const restaurantID = useGetRestaurantID();
  const data = useSelector(
    (state) => state.restaurants.singleRestaurantProducts
  );
  const [products, setProducts] = useState(data);

  const dispatch = useDispatch();

  // dispatching the restaurants products
  useEffect(() => {
    dispatch(getSingleRestaurantProducts(restaurantID));
  }, [products]);

  // deleting item function
  const handleDelete = (id, restaurantID) => {
    dispatch(
      deleteProduct({
        productID: id,
        restaurantID,
        cb: (data) => {
          if (data.success === "Item removed successfully") {
            dispatch(getSingleRestaurantProducts(restaurantID));
          }
        },
      })
    );
  };

  return (
    <>
      <Container>
        {data?.data?.length < 1 ? (
          <h1>Add products</h1>
        ) : (
          <>
            {data?.data?.map((item) => (
              <Card
                key={item._id}
                data={item}
                handleDelete={handleDelete}
                restaurantID={restaurantID}
              />
            ))}
          </>
        )}
      </Container>
      <Footer />
    </>
  );
};

export default AllProducts;
