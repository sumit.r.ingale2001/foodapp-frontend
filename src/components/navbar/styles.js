/* eslint-disable no-unused-vars */
import { AppBar, Box, Drawer, Toolbar, styled } from '@mui/material'


// main appbar which consist all of the elements 
export const Header = styled(AppBar)(({ theme }) => ({
    padding: "0.5rem 1rem",
    transition: "all 0.3s ease-in-out"
}))

// toolbar which consists the links and all the other stuffs 
export const StyledToolbar = styled(Toolbar)(({ theme }) => ({
    display: "flex",
    width: "100%",
    maxWidth: "1350px",
    margin: "0 auto",
    alignItems: "center",
    justifyContent: "space-between",
    gap: "1.5rem",
    "&  img": {
        width: "100px",
    }
}))

export const SearchContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    flex: 2,
    justifyContent: "space-between",
    alignItems: "center",
    background: "white",
    borderRadius: "3px",
    padding: "0 0.8rem",
    "& > input": {
        border: "none",
        background: "none",
        outline: "none",
        flex: 2
    },
}))

export const NavLinksContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    gap: "2rem",
    "& > a": {
        fontWeight: "bold",
        fontSize:"1.1rem",
    },
    [theme.breakpoints.down("md")]: {
        flexDirection: "column",
        alignItems: "start",
        marginTop: "1rem",
        "& > a": {
            color: "#5D76A9 !important",
            margin: "0.5rem 0"
        },
    },
}))

export const LinksContainer = styled(Box)(({ theme }) => ({
    [theme.breakpoints.down("md")]: {
        display: "none"
    }
}))

export const DrawerBtn = styled(Box)(({ theme }) => ({
    display: "none",
    [theme.breakpoints.down("md")]: {
        display: "block"
    },
}))


export const StyledDrawer = styled(Drawer)(({ theme }) => ({
    "& > div": {
        padding: "1rem 2rem",
        "& img": {
            margin: "0 1rem 1rem 0",
            width: "150px"
        }
    },
}))