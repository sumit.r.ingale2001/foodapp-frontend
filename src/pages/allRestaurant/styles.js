/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({ theme }) => ({
    width: "90%",
    margin: "5rem auto",
    "& > table td": {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        background: "#fff",
        color: "black",
        fontSize: "1.1rem",
        cursor: "pointer",
    },
    "& > table h5": {
        fontWeight: "bold",
        color: "#5d76a9"
    }
}))

export const ProductContainer = styled(Box)(({theme})=>({
    display:"flex",
    gap:"0.8rem",
    flexWrap:"wrap",
}))