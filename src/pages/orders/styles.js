/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({ theme }) => ({
    marginTop: "5rem",
    padding: "1rem",
    "& > table": {
        borderRadius: "10px",
        overflow: "hidden",
        maxWidth: "1200px",
        width: "95%",
        margin: "2rem auto",
        "& > thead > tr > th": {
            background: "#5d76a9",
            color: "white",
            textAlign: "center",
        },
        "& > tbody > tr > td": {
            textAlign: "center"
        },
        "& > tbody": {
            background: "white"
        },
    },
    "& > h2": {
        textAlign: "center"
    }
}))