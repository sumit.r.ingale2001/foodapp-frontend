/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import { Container, Empty } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import { deleteItemFromCart, getUserCart } from "../../store/builderFunctions";
import {
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from "@mui/material";
import { useGetUserID } from "../../hooks/useGetUserID";
import Total from "./Total";
import cartEmpty from "../../assets/images/cartEmpty.png";
import { Footer } from "../../components/index";

const Cart = () => {
  const dispatch = useDispatch();
  const userID = useGetUserID();
  const cart = useSelector((state) => state.restaurants.cart);
  const [isChanged, setIsChanged] = useState(cart);

  // handleDelete function
  const handleDelete = (id, userID) => {
    dispatch(
      deleteItemFromCart({
        productID: id,
        userID,
        cb: (data) => {
          if (data.success === "Removed from cart") {
            dispatch(getUserCart(userID));
          }
        },
      })
    );
  };

  // when the userID is present then only the dispatch will work
  useEffect(() => {
    if (userID) {
      dispatch(getUserCart(userID));
    }
  }, [isChanged]);

  return (
    <>
      {!userID || cart?.length < 2 ? (
        <Empty>
          <img src={cartEmpty} alt="cart empty" />
          <h3>Cart empty</h3>
        </Empty>
      ) : (
        <Container>
          <Grid container>
            <Grid item lg={8} md={8} sm={12} xs={12} sx={{ padding: "1rem" }}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Sr. no </TableCell>
                    <TableCell>Product </TableCell>
                    <TableCell>Product Name</TableCell>
                    <TableCell>Quantity</TableCell>
                    <TableCell>Total</TableCell>
                    <TableCell>Remove</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {cart?.slice(1).map((item, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell>{index + 1}</TableCell>
                        <TableCell>
                          <img src={item?.productImg} alt={item?.productName} />
                        </TableCell>
                        <TableCell>{item?.productName}</TableCell>
                        <TableCell>{item?.quantity}</TableCell>
                        <TableCell>{item?.total} Rs</TableCell>
                        <TableCell>
                          <Button
                            variant="contained"
                            onClick={() => handleDelete(item.productID, userID)}
                            color="error"
                          >
                            X
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Grid>

            <Grid item lg={4} md={4} sm={12} xs={12} sx={{ padding: "1rem" }}>
              <Total data={cart} userID={userID} />
            </Grid>
          </Grid>
        </Container>
      )}
      <Footer />
    </>
  );
};

export default Cart;
