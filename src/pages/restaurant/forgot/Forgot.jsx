/* eslint-disable no-unused-vars */
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Bg, Container, StyledPaper } from "../login/styles"
import { Box, Button, Checkbox, TextField, Typography } from "@mui/material";
import { changeRestaurantPassword } from "../../../services/api";
import { toast } from "react-toastify";



const initialValues = {
    email: "",
    password: "",
    confirmPassword: ""
}


const Forgot = () => {

    const navigate = useNavigate()
    const [formValues, setFormValues] = useState(initialValues);
    const [showPassword, setShowPassword] = useState(false);

    const changePass = async (e) => {
        e.preventDefault();
        const { data } = await changeRestaurantPassword(formValues);
        if (data) {
            if (data.error) {
                toast.error(data.error);
            } else if (data.info) {
                toast.info(data.info)
            } else {
                toast.success(data.success);
                navigate("/restaurantLogin")
            }
        } else {
            toast.error("Somwthing went wrong")
        }
    }

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }
    return (
        <Container>
            <StyledPaper>
                <Typography variant="h4" >Change password</Typography>
                <form>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            value={formValues.email}
                            name="email"
                            type="email"
                            label="Email" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.password}
                            name="password"
                            label="New password" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.confirmPassword}
                            name="confirmPassword"
                            label="Confirm password" />
                    </Box>
                    <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} >
                        <Box>
                            <Checkbox onClick={() => setShowPassword(!showPassword)} />
                            {showPassword ? "Hide password" : "Show password"}
                        </Box>
                    </Box>
                    <Box>
                        <Button variant="contained" fullWidth onClick={changePass} >Change password</Button>
                    </Box>
                </form>
            </StyledPaper>
            <Bg>
                <img src="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt="" />
            </Bg>
        </Container>
    )
}

export default Forgot
