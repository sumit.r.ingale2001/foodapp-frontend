/* eslint-disable no-unused-vars */

import { Box, Grid, styled } from '@mui/material'

export const Container = styled(Grid)(({ theme }) => ({
    padding: "5rem 0 0 0",
    backdropFilter: "blur(5px)",
}))

export const Bg = styled(Box)(({ theme }) => ({
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: -1,
    height: "100%",
    width: "100%",
    filter: "brightness(20%)",
    "& > img": {
        filter: "grayscale(1)",
        height: "100%",
        width: "100%",
        objectFit: "cover"
    }
}))

export const Left = styled(Grid)(({ theme }) => ({
    padding: "1rem",
    "&>img": {
        height: "80%",
        width: "80%",
        borderRadius:"20px",
        objectFit: "cover",
    },
}))

export const Right = styled(Grid)(({ theme }) => ({
    padding: "1rem",
    color: "white",
    display: "flex",
    flexDirection: "column",
    justifyContent:"center",
    gap: "1.5rem",
    "& > h4": {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        fontWeight: "bolder"
    },
    [theme.breakpoints.down("md")]: {
        marginTop: "3rem"
    }
}))


export const ButtonContainer = styled(Box)(({theme})=>({
    "& > span":{
        width:"80px",
        textAlign:"center"
    },
    display:"flex",
    alignItems:"center",
    borderRadius:"20px",
    overflow:"hidden"
}))