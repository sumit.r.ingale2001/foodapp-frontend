import { Box, Button, Checkbox, TextField, Typography } from "@mui/material"
import { Bg, Container, StyledPaper } from "./styles"
import { Link, useNavigate } from 'react-router-dom'
import { useState } from "react";
import { signupUser } from "../../../services/api";
import { toast } from 'react-toastify'


const Signup = () => {

    const navigate = useNavigate()

    const initialValues = {
        name: "",
        email: "",
        password: "",
        phone: "",
        profileImg: ""
    }

    const [formValues, setFormValues] = useState(initialValues);
    const [showPassword, setShowPassword] = useState(false)

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleSignup = async (e) => {
        e.preventDefault();
        const { data } = await signupUser(formValues);
        if (data) {
            if (data.info) {
                toast.info(data.info)
            } else if (data.error) {
                toast.error(data.error)
            } else {
                toast.success(data.success)
                navigate("/login")
            }
        } else {
            toast.error("Something went wrong")
        }
    }

    return (
        <Container>
            <StyledPaper>
                <Typography variant="h4" >Signup</Typography>
                <form>
                    <Box>
                        <TextField
                            fullWidth
                            value={formValues.name}
                            name="name"
                            type="text"
                            onChange={handleChange}
                            label="Name" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            value={formValues.email}
                            name="email"
                            type="email"
                            label="Email" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type="number"
                            value={formValues.phone}
                            name="phone"
                            label="Mobile number" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.password}
                            name="password"
                            label="Password" />
                    </Box>
                    <Box >
                        <Checkbox onClick={() => setShowPassword(!showPassword)} />
                        {showPassword ? "Hide password" : "Show password"}
                    </Box>
                    <Box>
                        <Button variant="contained" fullWidth onClick={handleSignup} >Signup</Button>
                    </Box>
                </form>
                <Typography>Already a member? <Link to="/login">Login</Link></Typography>
            </StyledPaper>
            <Bg>
                <img src="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt="" />
            </Bg>
        </Container>
    )
}

export default Signup
