/* eslint-disable no-unused-vars */
import { Box, Paper, styled } from "@mui/material";


export const StyledPaper = styled(Paper)(({theme})=>({
    padding:"0.3rem",
    width:"18rem",
    cursor:"pointer",
    display:"flex",
    flexDirection:"column",
    [theme.breakpoints.down("md")]:{
        width:"80%",
        height:"25rem"
    },
    height:"30rem"
}))

export const Image = styled(Box)(({theme})=>({
width:"100%",
height:"60%",
overflow:"hidden",
"& > img":{
    objectFit:"cover",
    height:"100%",
    width:"100%"
}
}))

export const Content = styled(Box)(({theme})=>({
    display: "flex",
    gap:"0.5rem",
    padding:"0.5rem",
    flexDirection:"column",
    "& > h6":{
        fontWeight:"bold"
    }
}))

export const Title = styled(Box)(({theme})=>({
    display:"flex",
    justifyContent:"space-between",
    alignItems:"center",
    "& > h6":{
        fontWeight:"bold",
        fontSize:"1rem"
    }
}))