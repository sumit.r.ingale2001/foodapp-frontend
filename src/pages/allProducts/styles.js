/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";

export const Container = styled(Box)(({ theme }) => ({
    margin: "5rem 0",
    padding: "0.5rem",
    display: "flex",
    flexWrap: "wrap",
    "& > h1": {
        textAlign: "center",
        width: "100%"
    },
    gap: "1rem",
    [theme.breakpoints.down("md")]: {
        justifyContent: "center"
    },
}))