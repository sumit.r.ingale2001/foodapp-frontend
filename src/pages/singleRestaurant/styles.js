/* eslint-disable no-unused-vars */
import { Box, styled } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({

    padding:"0 1.2rem",
    marginTop: "5rem",
    "& > h6": {
        fontWeight: "bold",
        fontSize:"1.2rem",
        color:"#5d76a9"
    }
}))

export const ProductContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    gap: "0.8rem",
    marginTop: '1rem',
    flexWrap: "wrap",
    [theme.breakpoints.down("md")]:{
        justifyContent:"center"
    }
}))
