import { useEffect } from "react"
import { Container } from "./styles"
import { useDispatch, useSelector } from 'react-redux'
import { getAllProducts } from "../../store/builderFunctions"
import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"
import ChevronRightIcon from '@mui/icons-material/ChevronRight';


const AllRestaurant = () => {

    const dispatch = useDispatch();
    const navigate = useNavigate()

    const allProducts = useSelector((state) => state.restaurants.allRestaurantProduct);

    useEffect(() => {
        dispatch(getAllProducts())
    }, [])


    return (
        <Container>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell sx={{ textAlign: "center" }} >
                            <Typography variant="h5" >All Restaurants</Typography>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody  >
                    {
                        allProducts.map((restaurant) => (
                            <TableRow key={restaurant.id} >
                                <TableCell onClick={() => navigate(`/singleRestaurant/${restaurant.id}`)} >
                                    {restaurant.restaurantName}
                                    <ChevronRightIcon />
                                </TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </Container>
    )
}

export default AllRestaurant
