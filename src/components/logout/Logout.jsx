/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Menu, MenuItem } from "@mui/material"
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import { useGlobalContext } from "../../context/context";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const Logout = ({ open, handleClose }) => {


    const openMenu = Boolean(open)

    // getting the role of the client start
    const { setRole } = useGlobalContext()
    // getting the role of the client end

    // accessing to the cookies start 
    const [cookies, setCookies, removeCookies] = useCookies(["access_token"]);
    // accessing to the cookies end

    const navigate = useNavigate()

    // logout function start, it will clear the cookies, local storage and will set the role to null
    const logout = () => {
        localStorage.clear()
        removeCookies("access_token");
        toast.success("Logged out successfully")
        setRole(null)
        navigate("/")
    }
    // logout function end

    return (
        <>
            {/* mui component menu start */}
            <Menu
                sx={{ marginTop: "0.8rem" }}
                anchorEl={open}
                open={openMenu}
                onClose={handleClose}
            >
                {/* logout button start  */}
                <MenuItem sx={{ padding: "0 3rem" }} onClick={logout} >
                    Logout&nbsp;<PowerSettingsNewIcon />
                    {/* logout button end */}
                </MenuItem>
            </Menu>
            {/* mui component menu end */}
        </>
    )
}

export default Logout
