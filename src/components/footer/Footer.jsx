import { Divider, Grid } from '@mui/material'
import { Container, Content, ContentContainer, Img } from './styles'
import logo from "../../assets/images/logo.png"
import { dineVerse, footerAbout, forEnterprises, forRestaurants, learnMore } from '../../assets/data/data'
import { Link } from 'react-router-dom'

const Footer = () => {
    return (
        <Container>
            {/* logo start  */}
            <Img>
                <img src={logo} alt="logo" />
            </Img>
            {/* logo end*/}

            <Grid container>
                <Grid item xs={12}>
                    <ContentContainer>

                        {/* about dine start  */}
                        <Content>
                            <h3>ABOUT DINE</h3>
                            {
                                footerAbout.map((item) => (
                                    <Link key={item.id}>{item.name}</Link>
                                ))
                            }
                        </Content>
                        {/* about dine end */}

                        {/* dine verse start  */}
                        <Content>
                            <h3>DINE VERSE</h3>
                            {
                                dineVerse.map((item) => (
                                    <Link key={item.id} >{item.name}</Link>
                                ))
                            }
                        </Content>
                        {/* dine verse end */}

                        {/* restaurants start  */}
                        <Content>
                            <h3>RESTAURANTS</h3>
                            {
                                forRestaurants.map((item) => (
                                    <Link key={item.id} >{item.name}</Link>
                                ))
                            }
                        </Content>
                        {/* restaurants end */}

                        {/* learn more start  */}
                        <Content>
                            <h3>LEARN MORE</h3>
                            {
                                learnMore.map((item) => (
                                    <Link key={item.id} >{item.name}</Link>
                                ))
                            }
                        </Content>
                        {/* learn more end */}

                        {/* enterprises start  */}
                        <Content>
                            <h3>ENTERPRISES</h3>
                            {
                                forEnterprises.map((item) => (
                                    <Link key={item.id}>{item.name}</Link>
                                ))
                            }
                        </Content>
                        {/* enterprises end */}
                    </ContentContainer>
                </Grid>
            </Grid>
            <Divider />
            
            <small>By continuing past this page, you agree to our Terms of Service, Cookie Policy, Privacy Policy and Content Policies. All trademarks are properties of their respective owners. 2008-2023 © Dine™ Ltd. All rights reserved.</small>
        </Container>
    )
}

export default Footer
