/* eslint-disable no-unused-vars */
import { Box, Paper, styled } from "@mui/material";


export const Contianer = styled(Box)(({theme})=>({
    maxWidth:"800px",
    display:"flex",
    width:"80%",
    position:"relative",
    background:"rgba(200, 206, 213, 1)",
    padding:"0 1.2rem",
    borderRadius:"5px",
    alignItems:"center",
    "& > input":{
        width:"100%",
        padding:"1.2rem 3rem",
        background:"none",
        outline:"none",
        border:"none",
        fontSize:"1.1rem"      
    },
}))

export const Wrapper = styled(Paper)(({theme})=>({
position:"absolute",
top:"4.5rem",
left:0,
width:"100%",
maxHeight:"18rem",
overflowY:"scroll",
"& img":{
    width:"60px"
},
"& li> div":{
    display:"flex",
    width:"100%",
    alignItems:"center",
        justifyContent:"space-between"
}
}))