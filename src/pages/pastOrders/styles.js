/* eslint-disable no-unused-vars */
import { styled, Box } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    marginTop: "5rem",
    "& > h2": {
        textAlign: "center"
    },
    "& > h1": {
        textAlign: "center",
        color: "#5d76a9",
        padding: "2rem 0"
    },
    "& > table": {
        borderRadius: "10px",
        overflow: "hidden",
        maxWidth: "1200px",
        width: "95%",
        margin: "2rem auto",
        "& > thead > tr > th": {
            background: "#5d76a9",
            color: "white",
            textAlign: "center",
        },
        "& > tbody > tr > td > img": {
            height: "50px",
            width: "50px",
            borderRadius: "50%",
            objectFit: "cover"
        },
        "& > tbody > tr > td": {
            textAlign: "center"
        },
        "& > tbody": {
            background: "white"
        },
    }
}))