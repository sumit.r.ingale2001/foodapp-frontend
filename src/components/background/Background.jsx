import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { carouselImages } from '../../assets/data/data';
import {  Container } from './styles';

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};
const Background = () => {
    return (
        <Container>
            <Carousel
                swipeable={false}
                draggable={false}
                responsive={responsive}
                ssr={true} // means to render carousel on server-side.
                infinite={true}
                autoPlay
                arrows={false}
                autoPlaySpeed={5000}
                customTransition="all .5"
                transitionDuration={500}
            >
                {
                    carouselImages.map((image, index) => (
                        <img src={image.img} alt="images" key={index} />
                    ))
                }

            </Carousel>
        </Container>
    )
}

export default Background
