import { configureStore } from "@reduxjs/toolkit";
import { RestaurantSlice } from "./restaurantSlice";


export const store = configureStore({
    reducer: {
        restaurants: RestaurantSlice.reducer
    }
})