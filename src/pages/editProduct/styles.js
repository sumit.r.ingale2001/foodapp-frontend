/* eslint-disable no-unused-vars */
import { Box, Paper, styled } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    minHeight: "100vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding:"5rem 1rem"
}))

export const StyledPaper = styled(Paper)(({ theme }) => ({
    padding: "1.5rem",
    width: "100%",
    maxWidth: "500px",
    "& > h5": {
        textAlign: "center",
        fontWeight: "bold",
    },
    "& > form": {
        display: "flex",
        justifyContent: "center",
        flexDirection: "column",
        width:"100%",
        gap:"1rem",
        margin:"1rem 0",
        "& > div":{
            width:"100%",
            display: "flex",
            justifyContent: "space-between",
            gap:"0.8rem",
            alignItems: "center",
        }
    }
}))