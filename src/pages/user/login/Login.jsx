/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { Box, Button, Checkbox, TextField, Typography } from "@mui/material"
import { Bg, Container, StyledPaper } from "./styles"
import { Link, useNavigate } from 'react-router-dom'
import { useState } from "react";
import { loginUser } from "../../../services/api";
import { toast } from 'react-toastify'
import { useCookies } from 'react-cookie'
import { useGlobalContext } from "../../../context/context";

const initialValues = {
    email: "",
    password: "",
}

const Login = () => {

    const { setRole } = useGlobalContext()
    const navigate = useNavigate()
    const [formValues, setFormValues] = useState(initialValues);
    const [showPassword, setShowPassword] = useState(false);
    const [cookies, setCookies] = useCookies(["access_token"])

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleLogin = async (e) => {
        e.preventDefault();
        const { data } = await loginUser(formValues);
        if (data) {
            if (data.error) {
                toast.error(data.error)
            } else if (data.info) {
                toast.info(data.info)
            } else {
                toast.success(data.success);
                localStorage.setItem("email", data.email);
                localStorage.setItem("userID", data.userID);
                localStorage.setItem("role", data.role);
                setCookies("access_token", data.token)
                setRole(data.role)
                navigate("/")
            }
        } else {
            toast.error("Something went wrong")
        }
    }

    return (
        <Container>
            <StyledPaper>
                <Typography variant="h4" >Login</Typography>
                <form>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            value={formValues.email}
                            name="email"
                            type="email"
                            label="Email" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.password}
                            name="password"
                            label="Password" />
                    </Box>
                    <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between", fontSize: "0.8rem" }} >
                        <Box>
                            <Checkbox onClick={() => setShowPassword(!showPassword)} />
                            {showPassword ? "Hide password" : "Show password"}
                        </Box>
                        <Typography sx={{ fontSize: "0.8rem" }} component={Link} to="/userForgot" >Forgot password?</Typography>
                    </Box>
                    <Box>
                        <Button variant="contained" fullWidth onClick={handleLogin} >Login</Button>
                    </Box>
                </form>
                <Typography>Not a member? <Link to="/signup">Signup</Link></Typography>
            </StyledPaper>
            <Bg>
                <img src="https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=870&q=80" alt="" />
            </Bg>
        </Container>
    )
}

export default Login
