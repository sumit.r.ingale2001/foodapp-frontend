/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Avatar, Badge } from "@mui/material"
import { NavLinksContainer } from "./styles"
import { Link } from 'react-router-dom'
import { useEffect, useState } from "react"
import { Logout } from '../index'
import { useGlobalContext } from "../../context/context"
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useSelector } from "react-redux"
import { useGetUserID } from "../../hooks/useGetUserID"

// These are the links for the navbar 
// 1. Initially all the link will be shown on the web app except the dashboard link
// 2. If someone registered as a user only the cart and all restaurant links will be shown
// 3. If a restaurant registered or logged in then only the dashboard will be shown to the admin or the restaurant owner 

const allLinks = [
    { name: "Home", to: "/" },
    { name: "Become a restaurant", to: "/restaurantLogin" },
    { name: "All restaurant", to: "/menu" },
    { name: "Cart", to: "/cart" },
    { name: "Login", to: "/login" },
]

const adminLinks = [
    { name: "Home", to: "/" },
    { name: "All restaurants", to: "/menu" },
    { name: "Dashboard", to: "/dashboard" },
    { name: "Create Item", to: "/create" },
]

const userLinks = [
    { name: "Home", to: "/" },
    { name: "All restaurants", to: "/menu" },
    { name: "Cart", to: "/cart" },
]


const Navlinks = () => {

    const [open, setOpen] = useState(null);
    const { role } = useGlobalContext();
    const userID = useGetUserID()

    const handleClose = () => {
        setOpen(null);
    }

    const handleOpen = (e) => {
        setOpen(e.currentTarget)
    }

    // getting the email from the localStorage 
    const initial = localStorage.getItem("email")


    // putting the email into a state 
    const [initialChar, setInitialChar] = useState(initial)


    // this useEffect will watch continuously on the initial i.e the email into the localStorage
    useEffect(() => {
        // getting the first letter of the email to show the initial letter into the avatar
        setInitialChar(initial?.charAt(0))
    }, [initial])


    const data = useSelector((state) => state.restaurants.cart)

    return (
        <NavLinksContainer>
            {// become a restaurant or restaurant login/register
                role === "user" || role === "admin" ? null : < Link to="/restaurantLogin" >Become a restaurant</Link>
            }

            {// link to menu 
                role === "admin" ? null : <Link to="/allRestaurants" >All restaurants</Link>
            }

            {// link to menu 
                role === "admin" ? null : <Link to="/menu" >Menu</Link>
            }

            {//link to cart 
                role === "admin" ? null : <Link to="/cart" ><Badge color="secondary" badgeContent={userID && data?.length ? data.length - 1 : 0}  >
                    Cart&nbsp;<ShoppingCartIcon />
                </Badge>
                </Link>
            }

            {/* links to past orders  */}
            {
                role === "user" && <Link to="/pastOrders" >Past Orders</Link>
            }

            {/* link to dashboard  */}
            {role === "admin" && <Link to="/orders">Orders</Link>}
            {role === "admin" && <Link to="/allProducts">All Products</Link>}

            {/* link to create product  */}
            {role === "admin" && <Link to="/create">Create item</Link>}

            {// profile
                !initial && !role ? <Link to="/login" >Login</Link> :
                    <Avatar onClick={handleOpen} sx={{ cursor: "pointer", background: "#5d76a9" }}>
                        {initialChar}
                    </Avatar>
            }

            {// logout button
                role &&
                <Logout open={open} handleClose={handleClose} />
            }
        </NavLinksContainer >
    )
}

export default Navlinks
