/* eslint-disable no-unused-vars */
import { Box, Button, Checkbox, TextField, Typography } from "@mui/material"
import {  Bg, Container, StyledPaper } from "./styles"
import { Link, useNavigate } from 'react-router-dom'
import { useState } from "react";
import { toast } from 'react-toastify'
import { signupRestaurant } from "../../../services/api";

const RestaurantSignup = () => {

    const navigate = useNavigate()

    const initialValues = {
        name: "",
        email: "",
        password: "",
        address: "",
        openingTime: "",
        closingTime: ""
    }

    const [formValues, setFormValues] = useState(initialValues);
    const [showPassword, setShowPassword] = useState(false)

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleSignup = async (e) => {
        e.preventDefault();
        const {data} = await signupRestaurant(formValues)
        if(data){
            if(data.error){
                toast.error(data.error)
            }else if (data.info){
                toast.info(data.info)
            }else{
                toast.success(data.success);
                navigate("/restaurantLogin")
            }
        }else{
            toast.error("Something went wrong")
        }
    }

    return (
        <Container>
            <StyledPaper>
                <Typography variant="h4" >Restaurant Signup</Typography>
                <form>
                    <Box>
                        <TextField
                            fullWidth
                            value={formValues.name}
                            name="name"
                            type="text"
                            onChange={handleChange}
                            label="Restaurant name" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            value={formValues.email}
                            name="email"
                            type="email"
                            label="Email" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type="text"
                            value={formValues.address}
                            name="address"
                            label="Address" />
                    </Box>
                    <Box sx={{ display: "flex", gap: "1rem", alignItems: "center" }} >
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type="time"
                            value={formValues.openingTime}
                            name="openingTime"
                            label="Opening Time" />
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type="time"
                            value={formValues.closingTime}
                            name="closingTime"
                            label="Closing time" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.password}
                            name="password"
                            label="Password" />
                    </Box>
                    <Box >
                        <Checkbox onClick={() => setShowPassword(!showPassword)} />
                        {showPassword ? "Hide password" : "Show password"}
                    </Box>
                    <Box>
                        <Button size="large" variant="contained" fullWidth onClick={handleSignup} >Signup</Button>
                    </Box>
                </form>
                <Typography>Already an admin? <Link to="/restaurantLogin">Login</Link></Typography>
            </StyledPaper>
            <Bg>
                <img src="https://images.unsplash.com/photo-1541508168132-0b1d81249c25?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80" alt="" />
            </Bg>
        </Container>
    )
}

export default RestaurantSignup;
