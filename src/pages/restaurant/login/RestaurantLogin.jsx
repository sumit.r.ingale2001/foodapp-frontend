/* eslint-disable no-unused-vars */
import { Box, Button, Checkbox, TextField, Typography } from "@mui/material"
import { Bg, Container, StyledPaper } from "./styles"
import { Link, useNavigate } from 'react-router-dom'
import { useState } from "react";
import { toast } from 'react-toastify'
import { loginRestaurant } from "../../../services/api";
import { useCookies } from "react-cookie";
import { useGlobalContext } from "../../../context/context";

const initialValues = {
    email: "",
    password: "",
}

const RestaurantLogin = () => {

    const navigate = useNavigate()
    const { setRole } = useGlobalContext()


    const [formValues, setFormValues] = useState(initialValues);
    const [showPassword, setShowPassword] = useState(false)
    const [cookies, setCookies] = useCookies(["access_token"])

    const handleChange = (e) => {
        setFormValues({ ...formValues, [e.target.name]: e.target.value })
    }

    const handleLogin = async (e) => {
        e.preventDefault();
        const { data } = await loginRestaurant(formValues);
        if (data) {
            if (data.error) {
                toast.error(data.error)
            } else {
                toast.success(data.success);
                setCookies("access_token", data.token)
                localStorage.setItem("restaurantID", data.restaurantID);
                localStorage.setItem("email", data.email);
                localStorage.setItem("role", data.role);
                setRole(data.role)
                navigate("/")
            }
        } else {
            toast.error("Something went wrong");
        }
    }

    return (
        <Container>
            <StyledPaper>
                <Typography variant="h4" >Restaurant Login</Typography>
                <form>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            value={formValues.email}
                            name="email"
                            type="email"
                            label="Email" />
                    </Box>
                    <Box>
                        <TextField
                            onChange={handleChange}
                            fullWidth
                            type={showPassword ? "text" : "password"}
                            value={formValues.password}
                            name="password"
                            label="Password" />
                    </Box>
                    <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between" }} >
                        <Box>
                            <Checkbox onClick={() => setShowPassword(!showPassword)} />
                            {showPassword ? "Hide password" : "Show password"}
                        </Box>
                        <Typography component={Link} to="/restaurantForgot" >Forgot password?</Typography>
                    </Box>
                    <Box>
                        <Button variant="contained" size="large" fullWidth onClick={handleLogin} >Login</Button>
                    </Box>
                </form>
                <Typography>Not a admin? <Link to="/restaurantSignup">Signup</Link></Typography>
            </StyledPaper>
            <Bg>
                <img src="https://images.unsplash.com/photo-1541508168132-0b1d81249c25?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=387&q=80" alt="" />
            </Bg>
        </Container>
    )
}

export default RestaurantLogin
