/* eslint-disable no-unused-vars */
import {styled, Box} from '@mui/material'

export const Container = styled(Box)(({theme})=>({
    display:"flex",
    flexDirection:"column",
    gap:"2rem",
    padding:"2rem 1.5rem",
    background:"rgb(93, 118, 159)",
    "& > small":{
        color:"lightgray",
        textAlign:"center"
    }
}))


export const Img = styled(Box)(({theme})=>({
    height:"100px",
    width:"100px",
    borderRadius:"50%",
    "& > img":{
        width:"100px",
    },
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    background:"rgb(202, 213, 223)"
}))

export const ContentContainer = styled(Box)(({theme})=>({
    display:"flex",
    gap:"3rem",
    flexWrap:"wrap"
}))

export const Content = styled(Box)(({theme})=>({
    display:"flex",
    gap:"1rem",
    flexDirection:"column",
    flex:1,
    padding:"0.5rem",
    "& > a":{
        color:"rgb(202, 213, 223)"
    }
}))