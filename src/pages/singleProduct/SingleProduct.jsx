import { useLocation, useParams } from 'react-router-dom'
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getSingleProduct } from "../../store/builderFunctions"
import { SingleProductCard } from "../../components"

const SingleProduct = () => {

    const { id } = useParams();
    const dispatch = useDispatch();

    const data = useSelector(state => state.restaurants.singleProduct)
    const {state} = useLocation()



    useEffect(() => {
        dispatch(getSingleProduct(id))
    }, [id])


    return (
        <>

            {
                data?.map((item) => (
                    <SingleProductCard item={item} key={item._id} restaurantID={state} />
                ))
            }
        </>
    )
}

export default SingleProduct
