/* eslint-disable react/prop-types */
import { useEffect, useState } from "react"
import Navlinks from "./Navlinks"
import { DrawerBtn, Header, LinksContainer, StyledToolbar } from "./styles"
import { Box, IconButton } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import Sidebar from "./Sidebar";
import logo from '../../assets/images/logo.png'
import { Link } from "react-router-dom";

const Navbar = () => {


    const [isScrolled, setIsScrolled] = useState(false);
    const [isOpen, setIsOpen] = useState(false);


    useEffect(() => {
        window.addEventListener("scroll", scroll);
        return () => window.removeEventListener("scroll", scroll)
    }, [])

    // scroll function to change the background color of the navbar 
    const scroll = () => {
        if (window.scrollY > 10) {
            setIsScrolled(true)
        } else {
            setIsScrolled(false)
        }
    }

    // handleClose to close the menu i.e the logout button 
    const handleClose = () => {
        setIsOpen(false)
    }

    return (
        <Header sx={{
            background: isScrolled ? "rgb(202, 213, 223)" : "none",
            boxShadow: isScrolled ? "rgba(0, 0, 0, 0.16) 0px 1px 4px;" : "none"
        }} >
            <StyledToolbar>

                {/* logo start */}
                <Box component={Link} to="/" >
                    <img src={logo} alt="logo" />
                </Box>
                {/* logo end */}


                {/* navlinks start */}
                <LinksContainer>
                    <Navlinks />
                </LinksContainer>
                {/* navlinks end */}

                {/* hamburger icon start  */}
                <DrawerBtn>
                    <IconButton onClick={() => setIsOpen(true)} >
                        <MenuIcon sx={{ color: isScrolled ? "#5D76A9" : "white" }} fontSize="large" />
                    </IconButton>
                </DrawerBtn>
                {/* hamburger icon start end */}

                {/* drawer for mobile screens start */}
                <Sidebar open={isOpen} handleClose={handleClose} />
                {/* drawer for mobile screens end */}

            </StyledToolbar>
        </Header>
    )
}

export default Navbar
