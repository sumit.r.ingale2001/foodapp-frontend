import axios from 'axios';

const URL = import.meta.env.VITE_BACKEND_URL;


export const addProduct = async (restaurantID, data) => {
    try {
        return await axios.post(`${URL}/restaurant/products/addProduct/${restaurantID}`, data)
    } catch (error) {
        console.log(error, "error while calling the addProduct api")
    }
}
