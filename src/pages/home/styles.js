/* eslint-disable no-unused-vars */
import { styled, Box } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    minHeight:"100vh",
    overflow:"hidden"
}))

export const BgContainer = styled(Box)(({ theme }) => ({
    position: "relative",
    height: "100vh",
    width: "100vw",
    display:"flex",
    gap:"1rem",
    justifyContent:"center",
    alignItems:"center",
    flexDirection:"column",
    "& > img":{
        width:"50%",
        filter:"drop-shadow(4px 4px 3px #FFF)"
    }

}))
export const Bottom = styled(Box)(({ theme }) => ({
    height: "7rem",
    zIndex:"-10",
    position: "absolute",
    left: 0,
    width: "100%",
    bottom: "0",
    background: "rgba(200, 210, 215)",
    maskImage: "linear-gradient(360deg, rgba(200, 206, 213, 1), transparent)",
    WebkitMaskImage: "linear-gradient(360deg, rgba(200, 206, 213,1), transparent)"
}))
