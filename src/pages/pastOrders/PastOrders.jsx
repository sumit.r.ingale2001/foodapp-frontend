import { Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material"
import { Container } from "./styles"
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from "react"
import { getPastOrders } from '../../store/builderFunctions'
import { useGetUserID } from '../../hooks/useGetUserID'

const PastOrders = () => {

    const dispatch = useDispatch()
    const id = useGetUserID()
    const data = useSelector((state) => state.restaurants.pastOrders)

    useEffect(() => {
        if (data?.length === 0) {
            dispatch(getPastOrders(id))
        }
    }, [data])




    return (
        <>
            <Container>
                {
                    data?.length < 1 || data === undefined ? (
                        <h1>No items ordered yet</h1>
                    ) : (
                        <>
                            <h2>Past Orders</h2>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Product</TableCell>
                                        <TableCell>Product Name</TableCell>
                                        <TableCell>Restaurant Name</TableCell>
                                        <TableCell>Quantity</TableCell>
                                        <TableCell>Total</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {
                                        data?.map((item, indx) => (
                                            <TableRow key={indx} >
                                                <TableCell>
                                                    <img src={item.productImg} alt="" />
                                                </TableCell>
                                                <TableCell>{item.productName}</TableCell>
                                                <TableCell>{item.restaurantName}</TableCell>
                                                <TableCell>{item.quantity}</TableCell>
                                                <TableCell>{item.price} Rs</TableCell>
                                            </TableRow>
                                        ))
                                    }
                                </TableBody>
                            </Table>
                        </>
                    )
                }
            </Container>
        </>
    )
}

export default PastOrders
