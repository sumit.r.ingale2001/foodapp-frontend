/* eslint-disable no-unused-vars */
import { Box, Paper, styled } from '@mui/material'

export const Container = styled(Box)(({ theme }) => ({
    margin: "5rem 0",
    "& > div": {
        width: "95%",
        margin:"0 auto"
    },
    "&  table": {
        margin: "0 auto",
        "& > thead > tr > th": {
            textAlign: "center"
        },
        "& > tbody tr td> img": {
            width: "50px",
            height: "50px",
            borderRadius: "50%",
            objectFit: "cover"
        },
        "& > tbody > tr > td": {
            textAlign: "center"
        }
    }
}))


export const StyledPaper = styled(Paper)(({ theme }) => ({
    padding: "1rem",
    display:"flex",
    width:"100%",
    flexDirection:"column",
    gap:"1rem",
    "& > p":{
        display:"flex",
        justifyContent:"space-between",
        alignItems:"center"
    },
    "& > button":{
        border:"none",
        padding:"0.5rem 1.5rem",
        outline:"none",
        background:"#5D76A9",
        color:"white",
        cursor:"pointer"
    }
}))


export const Empty = styled(Box)(({theme})=>({
    marginTop:"5rem",
    display:"flex",
    justifyContent:"center",
    alignItems:"center",
    minHeight:"80vh",
    flexDirection:"column",
    gap:"1rem",
    "& > img":{
        height:"250px",
        width:"250px",
        objectFit:"contain",
        mixBlendMode:"color-burn"
    }
}))