/* eslint-disable react/prop-types */
import { Button } from "@mui/material"
import { ButtonContainer } from "../../pages/singleProduct/styles"


const ButtonGroup = ({ item, increaseQuantity, decreaseQuantity, quantity }) => {

    return (
        // increase decrease buttons 
        <ButtonContainer>
            <Button sx={{ background: "#5d76a9" }} onClick={decreaseQuantity} variant="contained" >-</Button>
            <span  >{quantity}</span>
            <Button sx={{ background: "#5d76a9" }} onClick={() => increaseQuantity(item?.quantity)} variant="contained" >+</Button>
        </ButtonContainer>
    )
}

export default ButtonGroup
