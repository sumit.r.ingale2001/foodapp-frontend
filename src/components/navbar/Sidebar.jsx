/* eslint-disable react/prop-types */
import { Divider } from "@mui/material"
import Navlinks from "./Navlinks"
import { StyledDrawer } from "./styles"
import logo from '../../assets/images/logo.png'


const Sidebar = ({ open, handleClose }) => {
    return (
        <StyledDrawer open={open} onClose={handleClose}>
            <img src={logo} alt="logo"/>
            <Divider />
            <Navlinks />
        </StyledDrawer>
    )
}

export default Sidebar
