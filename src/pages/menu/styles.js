/* eslint-disable no-unused-vars */
import { Box, styled } from "@mui/material";


export const Container = styled(Box)(({theme})=>({
    margin:"5rem 0",
    padding:"1rem",
    display:"flex",
    flexWrap:"wrap",
    gap:"1rem",
    [theme.breakpoints.down("md")]:{
        justifyContent:"center"
    }
}))