import { useEffect } from "react"
import { Background, Footer, Search, TypeWriter } from "../../components/index"
import { BgContainer, Bottom, Container } from "./styles"
import { verifyRestaurant, verifyUser } from "../../services/api"
import { useCookies } from "react-cookie"
import { useGlobalContext } from "../../context/context"
import { toast } from 'react-toastify'
import { useNavigate } from "react-router-dom"

const Home = () => {

    const navigate = useNavigate()
    const [cookies] = useCookies(["access_token"])
    const { role } = useGlobalContext();


    useEffect(() => {
        const user = async () => {
            await verifyUser()
        }
        user()
    }, [cookies])

    useEffect(() => {
        const restaurant = async () => {
            const { data } = await verifyRestaurant()
            if (data.success) {
                toast.success(data.success);
                navigate("/dashboard")
            }
        }
        restaurant()
    }, [cookies])

    return (
        <>
            <Container>
                <BgContainer>
                    {/* background carousel start  */}
                    <Background />
                    {/* background carousel end */}

                    {/* type writing effect start */}
                    <TypeWriter />
                    {/* type writing effect end */}

                    {/* search start  */}
                    {
                        role == "admin" ? null : <Search />
                    }

                    {/* search end */}

                    {/* linear gradient start  */}
                    <Bottom></Bottom>
                    {/* linear gradient end */}
                </BgContainer>
                <Footer />
            </Container>
        </>
    )
}

export default Home
