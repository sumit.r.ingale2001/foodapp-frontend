/* eslint-disable react/prop-types */
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import { Container, StyledPaper } from "./styles";
import { Box } from "@mui/system";
import FileBase from "react-file-base64";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  getSingleRestaurantProducts,
  updateProduct,
} from "../../store/builderFunctions";
import { useGetRestaurantID } from "../../hooks/useGetUserID";
import { useNavigate } from "react-router-dom";

const EditForm = ({ data }) => {
  const [formValues, setFormValues] = useState(null);
  const [prices, setPrices] = useState([]);
  const dispatch = useDispatch();
  const restaurantID = useGetRestaurantID();
  const navigate = useNavigate();

  useEffect(() => {
    setFormValues(data);
    setPrices(data?.price);
  }, [data]);

  // this functions gets the half and full value from the restaurant and puts it into the price array
  const handlePrices = (e, index) => {
    console.log(e.target.value, index);
    const currentPrices = [...prices];
    currentPrices[index] = e.target.value;
    setPrices(currentPrices);
  };

  const handleChange = (e) => {
    setFormValues({ ...formValues, [e.target.name]: e.target.value });
  };

  const handleEdit = async (e) => {
    e.preventDefault();
    dispatch(
      updateProduct({
        data: formValues,
        restaurantID,
        cb: (data) => {
          if (data.success === "Product updated successfully") {
            dispatch(getSingleRestaurantProducts(restaurantID));
          }
        },
      })
    );
    navigate("/allProducts");
  };

  return (
    <>
      <Container>
        <StyledPaper>
          <Typography variant="h5">Edit Item</Typography>
          <form>
            <Box>
              {/* product name  */}
              <TextField
                fullWidth
                value={formValues?.productName}
                onChange={handleChange}
                name="productName"
                label="Product name"
              />
            </Box>
            <Box>
              {/* product desc  */}
              <TextField
                fullWidth
                value={formValues?.desc}
                onChange={handleChange}
                name="desc"
                label="Description"
              />
            </Box>
            <Box>
              {/* product quantity  */}
              <TextField
                fullWidth
                value={formValues?.quantity}
                name="quantity"
                onChange={handleChange}
                label="Quantity"
              />
            </Box>
            <Box>
              {/* product category  */}
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  label="category"
                  name="category"
                  value={formValues?.category}
                  onChange={handleChange}
                >
                  <MenuItem value="Non-veg">Non-veg</MenuItem>
                  <MenuItem value="Veg">Veg</MenuItem>
                </Select>
              </FormControl>
            </Box>
            <Box>
              {/* half price  */}
              <TextField
                fullWidth
                type="number"
                label="Price : Half"
                value={prices[0]}
                onChange={(e) => handlePrices(e, 0)}
              />

              {/* full price  */}
              <TextField
                onChange={(e) => handlePrices(e, 1)}
                fullWidth
                type="number"
                value={prices[1]}
                label="Price : Full"
              />
            </Box>

            {/* product image  */}
            <Box>
              <img
                src={formValues?.productImg}
                style={{ width: "100%", height: "10rem", objectFit: "cover" }}
              />
            </Box>
            <Typography sx={{ fontSize: "0.9rem", fontWeight: "bold" }}>
              Update item image
            </Typography>

            {/* update product image  */}
            <Box>
              <FileBase
                type="file"
                multiple={false}
                onDone={({ base64 }) =>
                  setFormValues({ ...formValues, productImg: base64 })
                }
              />
            </Box>

            {/* handle delete  */}
            <Box>
              <Button fullWidth variant="contained" onClick={handleEdit}>
                Edit
              </Button>
            </Box>
          </form>
        </StyledPaper>
      </Container>
    </>
  );
};

export default EditForm;
