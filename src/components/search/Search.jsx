/* eslint-disable no-unused-vars */
import { Box, IconButton, ListItem, MenuItem, Typography } from "@mui/material";
import { Contianer, Wrapper } from "./styles"
import SearchIcon from '@mui/icons-material/Search';
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from "react";
import { getAllProducts } from '../../store/builderFunctions'
import { Link, useNavigate } from 'react-router-dom'

const Search = () => {

    // creating a state for the input word 
    const [text, setText] = useState(null)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    // dispatching allproducts 
    useEffect(() => {
        dispatch(getAllProducts())
    }, [dispatch])

    // extracting the data from redux store 
    const allProducts = useSelector((state) => state.restaurants.allRestaurantProduct)

    // storing the word into the state 
    const getText = (word) => {
        setText(word)
    }

    // function to navigate along with some data 
    const handleNavigate = (id, restaurantID) => {
        setText(null);
        navigate(`product/${id}`, { state: restaurantID })
    }

    return (
        <>
            <Contianer>
                {/* input to search  */}
                <input
                    type="text"
                    placeholder="Search food items..."
                    value={text}
                    onChange={(e) => getText(e.target.value)} />
                <IconButton>
                    <SearchIcon fontSize="large" />
                </IconButton>

                {/* when the user types something into the input field then only the list will show  */}
                {
                    text && (
                        <Wrapper>
                            {
                                allProducts.map((prod) => (
                                    prod.products.filter((product) => (
                                        product?.productName.toLowerCase().includes(text.toLowerCase())
                                    )).map((product) => (
                                        <MenuItem key={product._id} >
                                            <Box onClick={() => handleNavigate(product._id, prod.id)} >
                                                <img src={product.productImg} alt="product" />
                                                <Typography>{product.productName}</Typography>
                                                <Typography>Half: {product?.price[0]}rs</Typography>
                                            </Box>
                                        </MenuItem>
                                    ))
                                ))
                            }

                        </Wrapper>
                    )
                }
            </Contianer>
        </>
    )
}

export default Search
