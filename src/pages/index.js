export { default as Home } from './home/Home'
export { default as Login } from './user/login/Login'
export { default as Signup } from './user/signup/Signup'
export { default as RestaurantLogin } from './restaurant/login/RestaurantLogin'
export { default as RestaurantSignup } from './restaurant/signup/RestaurantSignup'
export { default as RestaurantForgot } from './restaurant/forgot/Forgot'
export { default as UserForgot } from './user/forgot/Forgot'
export { default as Orders} from './orders/Orders'
export {default as Create} from './createItem/Create'
export {default as AllRestaurant} from './allRestaurant/AllRestaurant'
export {default as SingleRestaurant} from './singleRestaurant/SingleRestaurant'
export {default as SingleProduct} from './singleProduct/SingleProduct'
export {default as Menu} from './menu/Menu'
export {default as Cart } from './cart/Cart'
export {default as AllProducts} from './allProducts/AllProducts'
export {default as EditProduct} from './editProduct/Edit'
export {default as PastOrders} from './pastOrders/PastOrders'